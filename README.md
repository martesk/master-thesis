# Master Thesis

This gitlab repository containes scripts used to execute the pseudo mapping and differential expression analysis for my Masters thesis 

## Bash_scripts:
Contains all bash scripts used to prepare a transcriptome and decoy file and to perform pseudo mapping with the salmon software for all RNA seq data.

Link to knitted R-markdown file:
https://glcdn.githack.com/martesk/master-thesis/-/raw/main/html%20files/Knitted_Bash_scripts.html

## Gills:
Contains code for the preparation work and excecution of the differential expression analysis by both DESeq2 and EdgeR for all data from the gill tissue.

Link to knitted R-markdown file:
https://glcdn.githack.com/martesk/master-thesis/-/raw/main/html%20files/Knitted_Gills.html

## HGh:
Contains code for the preparation work and excecution of the differential expression analysis by both DESeq2 and EdgeR for all data from the hindgut tissue.

Link to knitted R-markdown file:
https://glcdn.githack.com/martesk/master-thesis/-/raw/main/html%20files/Knitted_HGh.html

## Liver:
Contains code for the preparation work and excecution of the differential expression analysis by both DESeq2 and EdgeR for all data from the liver tissue.

Link to knitted R-markdown file:
https://glcdn.githack.com/martesk/master-thesis/-/raw/main/html%20files/Knitted_liver.html
